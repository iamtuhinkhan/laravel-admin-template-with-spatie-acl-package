<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\User;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        // $role = Role::create(['name' => 'user']);
        // // $role= Role::findById(1);
        // // $role->givePermissionTo(2);
        // $permission = Permission::create(['name' => 'add blog']);

         // auth()->user()->givePermissionTo('add blog');
         //auth()->user()->assignRole('user');

        // return  auth()->user()->permission('create user')->get();;
        
            return view('home');
        
    }
}
